// Aim'a.
// Based on SpeedySloth by Apple Inc.
// Licensed under the GNU Affero General Public License.
// Created by Leonid Provorov on 20/11/15, Alupka, Crimea.
// In memory of my farther Alexander Provorov (1956 - 2009).
// Copyright © 2019 Leonid Provorov. All rights reserved.

// * Project configuration and code is under development to convenient 1.1 version.

import WatchKit
import Foundation
import HealthKit
import WatchConnectivity

enum WatchTypes {
    case watch38
    case watch40
    case watch42
    case watch44
}

extension WKInterfaceDevice {
    
    class func getCurrentWatch() -> WatchTypes {
        
        let screenBounds: [String : CGRect] = [
            "Apple Watch 38mm" : CGRect(x: 0, y: 0, width: 272, height: 340),
            "Apple Watch 40mm" : CGRect(x: 0, y: 0, width: 324, height: 394),
            "Apple Watch 42mm" : CGRect(x: 0, y: 0, width: 312, height: 390),
            "Apple Watch 44mm" : CGRect(x: 0, y: 0, width: 368, height: 448)
        ]
        
        let currentScreenBounds: CGRect = WKInterfaceDevice.current().screenBounds
        
        switch currentScreenBounds {
            
        case screenBounds["Apple Watch 38mm"]:
            return .watch38
            
        case screenBounds["Apple Watch 40mm"]:
            return .watch40
            
        case screenBounds["Apple Watch 44mm"]:
            return .watch44
            
        default:
            return .watch42
            
        }
    }
    
}

class Database {

    // Database - Major Constants and Variables - Database
    let database: UserDefaults = UserDefaults.standard
    
    var hasData: Int!
    
    var userPower: [String : Any] = [
        "Air Power" : String(),
        "Earth Power" : String(),
        "Fire Power" : String()
    ]
    
    var airToPower: [String : Any] = [
        "Air Power" : String.self
    ]
    
    var earthToPower: [String : Any] = [
        "Earth Power" : String.self
    ]
    var fireToPower: [String : Any] = [
        "Fire Power" : String.self
    ]

    var airPower: [String : Any] = [
        "Air: average time" : Int(),
        "Air: average power" : Double(),
        "Air: average pulse" : Int()
    ]

    var earthPower: [String : Any] = [
        "Earth: average time" : Int(),
        "Earth: average power" : Double(),
        "Earth: average pulse" : Int()
    ]

    var firePower: [String : Any] = [
        "Fire: average time" : Int(),
        "Fire: average power" : Double(),
        "Fire: average pulse" : Int()
    ]

    // Database - Major Functions - Database
    
    func write(hasSentData: Int?) {
        if hasSentData == 0 {
            database.set(hasSentData, forKey: "0")
        }
        
        if hasSentData == 1 {
            database.set(hasSentData, forKey: "1")
        }
        
        if hasSentData == 2 {
             database.set(hasSentData, forKey: "2")
        }
        
    }
    
    
    
    
    
    func writeToDatabase(airPower: String?, earthPower: String?, firePower: String?) {
        
        if airPower != nil {
            database.set(airPower, forKey: "Air Power")
        } else { return }
        
        if earthPower != nil {
            database.set(earthPower, forKey: "Earth Power")
        } else { return }
        
        if firePower != nil {
            database.set(firePower, forKey: "Fire Power")
        } else { return }
        
    }
    
    
    func writeToDatabase(airAverageTime: Int?, airAveragePower: Double?, airAveragePulse: Int?) {

        if airAverageTime != nil {
            database.set(airAverageTime, forKey: "Air: average time")
        } else { return }

        if airAveragePower != nil {
            database.set(airAveragePower, forKey: "Air: average power")
        } else { return }

        if airAveragePulse != nil {
            database.set(airAveragePulse, forKey: "Air: average pulse")
        } else { return }

    }

    func writeToDatabase(earthAverageTime: Int?, earthAveragePower: Double?, earthAveragePulse: Int?) {

        if earthAverageTime != nil {
            database.set(earthAverageTime, forKey: "Earth: average time")
        } else { return }

        if earthAveragePower != nil {
            database.set(earthAveragePower, forKey: "Earth: average power")
        } else { return }

        if earthAveragePulse != nil {
            database.set(earthAveragePulse, forKey: "Earth: average pulse")
        } else { return }

    }

    func writeToDatabase(fireAverageTime: Int?, fireAveragePower: Double?, fireAveragePulse: Int?) {

        if fireAverageTime != nil {
            database.set(fireAverageTime, forKey: "Fire: average time")
        } else { return }

        if fireAveragePower != nil {
            database.set(fireAveragePower, forKey: "Fire: average power")
        } else { return }

        if fireAveragePulse != nil {
            database.set(fireAveragePulse, forKey: "Fire: average pulse")
        } else { return }

    }
    
}

var watch: String = "Apple Watch"

var interfaceColor: String = "Interface Color"

var userPower: String = "User Power"

var userAge: Int = 0

var userProtein: Double = 0

var userRetouchCatalyst: Bool = false

var userReturnsToPreviousInterface: Bool = false

var userShouldAcceptHealthStoreAuthorization = true

var firstWorkout: Bool = false

var indicator: Bool = true

func getPowerPulse() -> Int {
    
    var maxPulse: Int = 0
    
    if userAge != 0 {
        maxPulse = 220 - userAge
    }
    
    return maxPulse
    
}

// HealthKit - Major Constants and Variables - H
var workout: HKWorkoutSession!
var liveWorkoutBuilder: HKLiveWorkoutBuilder!

// System - Major Constants and Variables - S

var userReachComputationPoint: Bool = false
var isReach: Date = Date(timeIntervalSinceNow: 120)
var checkPulse: Bool = true
var oneSound: Bool = true

// Algorithm - Major Constants and Variables - A
var computationTimer: Timer = Timer()
var computationCycles: Int = 60

var indicatorTimer: Timer = Timer()

// Database - Major Constants and Variables - D
var workoutAverageTime: Int = 0
var userAveragePower: Double = 0.00
var userAveragePulse: Int = 0

var userTakePulse: Int = 0
var userFixedPulse: Int = 0
