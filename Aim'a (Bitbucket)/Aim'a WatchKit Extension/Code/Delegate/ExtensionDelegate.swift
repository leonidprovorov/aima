// Aim'a.
// Based on SpeedySloth by Apple Inc.
// Licensed under the GNU Affero General Public License.
// Created by Leonid Provorov on 20/11/15, Alupka, Crimea.
// In memory of my farther Alexander Provorov (1956 - 2009).
// Copyright © 2019 Leonid Provorov. All rights reserved.

// * Project configuration and code is under development to convenient 1.1 version.

import WatchKit

class ExtensionDelegate: NSObject, WKExtensionDelegate {

    func applicationDidFinishLaunching() {
        
        switch WKInterfaceDevice.getCurrentWatch() {
            
        case .watch38:
            watch = "Apple Watch 38mm"
            
        case .watch40:
            watch = "Apple Watch 40mm"
            
        case .watch42:
            watch = "Apple Watch 42mm"
    
        default:
            watch = "Apple Watch 44mm"
            
        }
    }

    func applicationDidBecomeActive() { }

    func applicationWillResignActive() { }

    func handle(_ backgroundTasks: Set<WKRefreshBackgroundTask>) {

        for task in backgroundTasks {
            
            switch task {
                
            case let backgroundTask as WKApplicationRefreshBackgroundTask:
                backgroundTask.setTaskCompletedWithSnapshot(false)
                
            case let snapshotTask as WKSnapshotRefreshBackgroundTask:
                snapshotTask.setTaskCompleted(restoredDefaultState: true, estimatedSnapshotExpiration: Date.distantFuture, userInfo: nil)
                
            case let connectivityTask as WKWatchConnectivityRefreshBackgroundTask:
                connectivityTask.setTaskCompletedWithSnapshot(false)
                
            case let urlSessionTask as WKURLSessionRefreshBackgroundTask:
                urlSessionTask.setTaskCompletedWithSnapshot(false)
                
            case let relevantShortcutTask as WKRelevantShortcutRefreshBackgroundTask:
                relevantShortcutTask.setTaskCompletedWithSnapshot(false)
                
            case let intentDidRunTask as WKIntentDidRunRefreshBackgroundTask:
                intentDidRunTask.setTaskCompletedWithSnapshot(false)
                
            default:
                task.setTaskCompletedWithSnapshot(false)
                
            }
        }
        
    }
}
