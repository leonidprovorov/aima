// Aim'a.
// Based on SpeedySloth by Apple Inc.
// Licensed under the GNU Affero General Public License.
// Created by Leonid Provorov on 20/11/15, Alupka, Crimea.
// In memory of my farther Alexander Provorov (1956 - 2009).
// Copyright © 2019 Leonid Provorov. All rights reserved.

// * Project configuration and code is under development to convenient 1.1 version.

import WatchKit
import Foundation

class Colorization: WKInterfaceController {
    
    // Storyboard - IBOutles - Storyboard
    @IBOutlet weak var silver: WKInterfaceButton!
    @IBOutlet weak var gray: WKInterfaceButton!
    @IBOutlet weak var gold: WKInterfaceButton!
    
    // Storyboard - IBActions - Storyboard
    @IBAction func whiteSilver() {
        interfaceColor = "White Silver"
        popToRootController()
    }
    
    @IBAction func spaceGray() {
        interfaceColor = "Space Gray"
        popToRootController()
    }
    
    @IBAction func pinkGold() {
        interfaceColor = "Pink Gold"
        popToRootController()
    }
    
    // Colorization - Initialization - Colorization
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        userReturnsToPreviousInterface = true
        
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
}
