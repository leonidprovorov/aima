// Aim'a.
// Based on SpeedySloth by Apple Inc.
// Licensed under the GNU Affero General Public License.
// Created by Leonid Provorov on 20/11/15, Alupka, Crimea.
// In memory of my farther Alexander Provorov (1956 - 2009).
// Copyright © 2019 Leonid Provorov. All rights reserved.

// * Project configuration and code is under development to convenient 1.1 version.

import WatchKit
import Foundation
import HealthKit

class Confirmation: WKInterfaceController {
 
    // Storyboard - IBOutles - Storyboard
    @IBOutlet weak var waterEnergy: WKInterfaceLabel!
    
    @IBOutlet weak var proteinEnergy: WKInterfacePicker!
    
    @IBOutlet weak var locomotion: WKInterfaceSwitch!
    
    // Storyboard - IBActions - Storyboard
    @IBAction func protein(_ value: Int) {
        
        let item = items[value]
        
        if let value = Int(item.title!) {
            userProtein = Double(value)
        }
        
    }
    
    @IBAction func locomotive(_ value: Bool) {
        WKInterfaceDevice.current().play(.click)
        getComputation()
    }
    
    // System - Major Functions - System
    func getComputation() {
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(computation), userInfo: nil, repeats: false)
    }
    
    @objc func computation() {
        presentController(withName: "Computation", context: interfaceColor)
    }
    
    // NSObject - Major Variables and Functions - NSObject
    var items: [WKPickerItem] = [WKPickerItem]()
    
    func getProteinEnergy() {
        
        items = (250...500).map {
            let pickerItem: WKPickerItem = WKPickerItem()
            pickerItem.title = "\($0)"
            pickerItem.caption = "Protein Energy Value"
            return pickerItem
        }
        
        proteinEnergy.setItems(items)
        
    }
    
    // HealthKit - Major Functions - HealthKit
    func requestHealthStoreAuthorization() {
        
        let healthStore: HKHealthStore = HKHealthStore()
        
        let typesToShare: Set = [HKSampleType.workoutType()]
        
        let typesToRead: Set = [
            HKQuantityType.quantityType(forIdentifier: .activeEnergyBurned)!,
            HKQuantityType.quantityType(forIdentifier: .heartRate)!
        ]
        
        healthStore.requestAuthorization(toShare: typesToShare, read: typesToRead) { (success, error) in }
        
    }
    
    // Confirmation - Initialization - Confirmation
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        switch context as? String {

        case "Earth Power":
            let moss: UIColor = UIColor(red: 0/255, green: 144/255, blue: 81/255, alpha: 1.0)
            
            waterEnergy.setText("WATER")
            locomotion.setColor(moss)

        case "Fire Power":
            let customRed: UIColor = UIColor(red: 205/255, green: 10/255, blue: 55/255, alpha: 1.0)
            
            getProteinEnergy()
            
            waterEnergy.setHidden(true)
            proteinEnergy.setHidden(false)
            
            locomotion.setColor(customRed)

        default:
            let aqua: UIColor = UIColor(red: 0/255, green: 150/255, blue: 255/255, alpha: 1.0)
            
            waterEnergy.setText("WATER")
            locomotion.setColor(aqua)

        }
        
        if HKHealthStore.isHealthDataAvailable() {
            
            if userShouldAcceptHealthStoreAuthorization == true {
                
                requestHealthStoreAuthorization()
                
                userShouldAcceptHealthStoreAuthorization = false
                
            }
        }
        
        self.crownSequencer.isHapticFeedbackEnabled = false
        
    }
    
    override func willActivate() {
        super.willActivate()
        
        
        if userRetouchCatalyst == true {
            
            indicatorTimer.invalidate()
            
            userAveragePulse = 0
            
            oneSound = true
            checkPulse = true
            
            if userReachComputationPoint == true {
                userTakePulse = 0
                workoutAverageTime = 0
                userAveragePower = 0.00
                computationTimer.invalidate()
                computationCycles = 60
                userReachComputationPoint = false
            }
            
            locomotion.setOn(false)
            
            workout.end()
            
            liveWorkoutBuilder.endCollection(withEnd: Date()) { (success, error) in
                liveWorkoutBuilder.finishWorkout { (workout, error) in }
            }
            
        
            userRetouchCatalyst = false
            
        }
        
        userReturnsToPreviousInterface = false
        
        if firstWorkout == true {
            popToRootController()
        }
        
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
}
