// Aim'a.
// Based on SpeedySloth by Apple Inc.
// Licensed under the GNU Affero General Public License.
// Created by Leonid Provorov on 20/11/15, Alupka, Crimea.
// In memory of my farther Alexander Provorov (1956 - 2009).
// Copyright © 2019 Leonid Provorov. All rights reserved.

// * Project configuration and code is under development to convenient 1.1 version.

import WatchKit
import Foundation

class Instruction: WKInterfaceController {
    
    // Storyboard - IBOutles - Storyboard
    @IBOutlet weak var decreaseWeight: WKInterfaceButton!
    @IBOutlet weak var stabilizeWeight: WKInterfaceButton!
    @IBOutlet weak var increaseWeight: WKInterfaceButton!
    
    // Storyboard - IBActions - Storyboard
    @IBAction func decrease() {
        userPower = "Air Power"
        getConfirmation()
    }
    
    @IBAction func stabilize() {
        userPower = "Earth Power"
        getConfirmation()
    }
    
    @IBAction func increase() {
        userPower = "Fire Power"
        getConfirmation()
    }
    
    // System - Major Functions - System
    func getConfirmation() {
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(confirmation), userInfo: nil, repeats: false)
    }
    
    @objc func confirmation() {
        pushController(withName: "Confirmation", context: userPower)
    }
    
    // Instruction - Initialization - Instruction
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        switch watch {
            
        case "Apple Watch 38mm":
            
            switch context as? String {
                
                case "Space Gray":
                    decreaseWeight.setBackgroundImageNamed("Decrease Weight (Gray 38mm)")
                    stabilizeWeight.setBackgroundImageNamed("Stabilize Weight (Gray 38mm)")
                    increaseWeight.setBackgroundImageNamed("Increase Weight (Gray 38mm)")
                
                case "Pink Gold":
                    decreaseWeight.setBackgroundImageNamed("Decrease Weight (Gold 38mm)")
                    stabilizeWeight.setBackgroundImageNamed("Stabilize Weight (Gold 38mm)")
                    increaseWeight.setBackgroundImageNamed("Increase Weight (Gold 38mm)")
                
                default:
                    decreaseWeight.setBackgroundImageNamed("Decrease Weight (Silver 38mm)")
                    stabilizeWeight.setBackgroundImageNamed("Stabilize Weight (Silver 38mm)")
                    increaseWeight.setBackgroundImageNamed("Increase Weight (Silver 38mm)")
                
            }
            
        case "Apple Watch 40mm":
            
            switch context as? String {
                
                case "Space Gray":
                    decreaseWeight.setBackgroundImageNamed("Decrease Weight (Gray 40mm)")
                    stabilizeWeight.setBackgroundImageNamed("Stabilize Weight (Gray 40mm)")
                    increaseWeight.setBackgroundImageNamed("Increase Weight (Gray 40mm)")
                
                case "Pink Gold":
                    decreaseWeight.setBackgroundImageNamed("Decrease Weight (Gold 40mm)")
                    stabilizeWeight.setBackgroundImageNamed("Stabilize Weight (Gold 40mm)")
                    increaseWeight.setBackgroundImageNamed("Increase Weight (Gold 40mm)")
                
                default:
                    decreaseWeight.setBackgroundImageNamed("Decrease Weight (Silver 40mm)")
                    stabilizeWeight.setBackgroundImageNamed("Stabilize Weight (Silver 40mm)")
                    increaseWeight.setBackgroundImageNamed("Increase Weight (Silver 40mm)")
                
            }
            
        case "Apple Watch 42mm":
            
            switch context as? String {
                
                case "Space Gray":
                    decreaseWeight.setBackgroundImageNamed("Decrease Weight (Gray 42mm)")
                    stabilizeWeight.setBackgroundImageNamed("Stabilize Weight (Gray 42mm)")
                    increaseWeight.setBackgroundImageNamed("Increase Weight (Gray 42mm)")
                
                case "Pink Gold":
                    decreaseWeight.setBackgroundImageNamed("Decrease Weight (Gold 42mm)")
                    stabilizeWeight.setBackgroundImageNamed("Stabilize Weight (Gold 42mm)")
                    increaseWeight.setBackgroundImageNamed("Increase Weight (Gold 42mm)")
                
                default:
                    decreaseWeight.setBackgroundImageNamed("Decrease Weight (Silver 42mm)")
                    stabilizeWeight.setBackgroundImageNamed("Stabilize Weight (Silver 42mm)")
                    increaseWeight.setBackgroundImageNamed("Increase Weight (Silver 42mm)")
                
            }
            
        default:
            
            switch context as? String {
                
                case "Space Gray":
                    decreaseWeight.setBackgroundImageNamed("Decrease Weight (Gray 44mm)")
                    stabilizeWeight.setBackgroundImageNamed("Stabilize Weight (Gray 44mm)")
                    increaseWeight.setBackgroundImageNamed("Increase Weight (Gray 44mm)")
                
                case "Pink Gold":
                    decreaseWeight.setBackgroundImageNamed("Decrease Weight (Gold 44mm)")
                    stabilizeWeight.setBackgroundImageNamed("Stabilize Weight (Gold 44mm)")
                    increaseWeight.setBackgroundImageNamed("Increase Weight (Gold 44mm)")
                
                default:
                    decreaseWeight.setBackgroundImageNamed("Decrease Weight (Silver 44mm)")
                    stabilizeWeight.setBackgroundImageNamed("Stabilize Weight (Silver 44mm)")
                    increaseWeight.setBackgroundImageNamed("Increase Weight (Silver 44mm)")
                
            }
        }
    }
    
    override func willActivate() {
        super.willActivate()
        
        userReturnsToPreviousInterface = true
        
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
}
