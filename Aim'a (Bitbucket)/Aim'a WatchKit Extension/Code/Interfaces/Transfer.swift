// Aim'a.
// Based on SpeedySloth by Apple Inc.
// Licensed under the GNU Affero General Public License.
// Created by Leonid Provorov on 20/11/15, Alupka, Crimea.
// In memory of my farther Alexander Provorov (1956 - 2009).
// Copyright © 2019 Leonid Provorov. All rights reserved.

// * Project configuration and code is under development to convenient 1.1 version.

import WatchKit
import Foundation
import WatchConnectivity

class Transfer: WKInterfaceController, WCSessionDelegate {
    
    // Storyboard - IBOutles - Storyboard
    @IBOutlet weak var power: WKInterfaceLabel!
    @IBOutlet weak var catalyst: WKInterfaceLabel!
    
    @IBOutlet weak var transfer: WKInterfaceButton!
    
    // Storyboard - IBActions - Storyboard
    @IBAction func transferData() {
        
        transferDatabase()
        
        if session.isReachable {
            WKInterfaceDevice.current().play(.success)
            getBase()
            cleanDatabase()
        } else {
            WKInterfaceDevice.current().play(.retry)
        }
        
    }
    
    // Database - Major Constants and Variables - Database
    let database: Database = Database()
    
    // WatchConnectivity - Major Constants and Variables - WatchConnectivity
    var session: WCSession = WCSession.default
    
    // Database - Major Functions - Database
    func cleanDatabase() {
        
        if userPower == "Air Power" {
            database.database.removeObject(forKey: "0")
            
            database.database.removeObject(forKey: "Air: average time")
            database.database.removeObject(forKey: "Air: average power")
            database.database.removeObject(forKey: "Air: average pulse")
        }
        
        if userPower == "Earth Power" {
            database.database.removeObject(forKey: "1")
            
            database.database.removeObject(forKey: "Earth: average time")
            database.database.removeObject(forKey: "Earth: average power")
            database.database.removeObject(forKey: "Earth: average pulse")
        }
        
        if userPower == "Fire Power" {
            database.database.removeObject(forKey: "2")
            
            database.database.removeObject(forKey: "Fire: average time")
            database.database.removeObject(forKey: "Fire: average power")
            database.database.removeObject(forKey: "Fire: average pulse")
        }
    }
    
    
    func getDatabase() {
        
        if userPower == "Air Power" {
            print("Air")
            //database.airToPower["Air Power"] = database.database.string(forKey: "Air Power")
            database.hasData = database.database.integer(forKey: "0")
            
            database.airPower["Air: average time"] = database.database.integer(forKey: "Air: average time")
            database.airPower["Air: average power"] = database.database.double(forKey: "Air: average power")
            database.airPower["Air: average pulse"] = database.database.integer(forKey: "Air: average pulse")
        }
        
        if userPower == "Earth Power" {
             print("Earth")
           // database.earthToPower["Earth Power"] = database.database.string(forKey: "Earth Power")
            
            database.hasData = database.database.integer(forKey: "1")
            
            database.earthPower["Earth: average time"] = database.database.integer(forKey: "Earth: average time")
            database.earthPower["Earth: average power"] = database.database.double(forKey: "Earth: average power")
            database.earthPower["Earth: average pulse"] = database.database.integer(forKey: "Earth: average pulse")
        }
        
        if userPower == "Fire Power" {
             print("Fire")
           // database.fireToPower["Fire Power"] = database.database.string(forKey: "Fire Power")
            
            database.hasData = database.database.integer(forKey: "2")
            
            database.firePower["Fire: average time"] = database.database.integer(forKey: "Fire: average time")
            database.firePower["Fire: average power"] = database.database.double(forKey: "Fire: average power")
            database.firePower["Fire: average pulse"] = database.database.integer(forKey: "Fire: average pulse")
        }
        
    }
    
    func transferDatabase() {
        
        //session.transferUserInfo(database.userPower)
    
        if userPower == "Air Power" {
            //session.transferUserInfo(database.airToPower)
            session.transferUserInfo(["0" : database.hasData!] as [String : Any])
            session.transferUserInfo(database.airPower)
        }
        
        if userPower == "Earth Power" {
            //session.transferUserInfo(database.earthToPower)
            session.transferUserInfo(["1" : database.hasData!] as [String : Any])
            session.transferUserInfo(database.earthPower)
        }
        
        if userPower == "Fire Power" {
            //session.transferUserInfo(database.fireToPower)
            session.transferUserInfo(["2" : database.hasData!] as [String : Any])
            session.transferUserInfo(database.firePower)
        }
        
    }
    
    // System - Major Functions - System
    func getBase() {
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(base), userInfo: nil, repeats: false)
    }
    
    @objc func base() {
        popToRootController()
    }
    
    // WatchConnectivity - Major Functions - WatchConnectivity
    func getSessionStatus() {
        
        if WCSession.isSupported() {
            session.delegate = self
            session.activate()
        }
        
    }
    
    // WCSessionDelegate - Major Functions - WCSessionDelegate
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) { }

    // Transfer - Initialization - Transfer
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        switch context as? String {
            
        case "Earth Power":
            let moss: UIColor = UIColor(red: 0/255, green: 144/255, blue: 81/255, alpha: 1.0)
            
            power.setText("EARTH POWER:")
            catalyst.setText("WATER")
            
            catalyst.setTextColor(moss)
            
        case "Fire Power":
            let customRed: UIColor = UIColor(red: 205/255, green: 10/255, blue: 55/255, alpha: 1.0)
            
            power.setText("FIRE POWER:")
            catalyst.setText("PROTEIN")
            
            catalyst.setTextColor(customRed)
            
        default:
            let aqua: UIColor = UIColor(red: 0/255, green: 150/255, blue: 255/255, alpha: 1.0)
            
            power.setText("AIR POWER:")
            catalyst.setText("WATER")
            
            catalyst.setTextColor(aqua)
            
        }
        
        getDatabase()
        
        userReturnsToPreviousInterface = true
        
        firstWorkout = false
    
    }
    
    override func willActivate() {
        super.willActivate()
        
        getSessionStatus()
        
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
}
