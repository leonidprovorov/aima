// Aim'a.
// Based on SpeedySloth by Apple Inc.
// Licensed under the GNU Affero General Public License.
// Created by Leonid Provorov on 20/11/15, Alupka, Crimea.
// In memory of my farther Alexander Provorov (1956 - 2009).
// Copyright © 2019 Leonid Provorov. All rights reserved.

// * Project configuration and code is under development to convenient 1.1 version.

import WatchKit
import Foundation
import WatchConnectivity

class Base: WKInterfaceController {
    
    // Storyboard - IBOutles - Storyboard
    @IBOutlet weak var aima: WKInterfaceLabel!
    
    @IBOutlet weak var tapGesture: WKTapGestureRecognizer!
    
    // Storyboard - IBActions - Storyboard
    @IBAction func tapGestureRecognizer(_ sender: Any) {
        pushController(withName: "Colorization", context: nil)
        tapGesture.isEnabled = false
    }
    
    // System - Major Functions - System
    func getInstruction() {
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(instruction), userInfo: nil, repeats: false)
    }
    
    func getTransfer() {
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(transfer), userInfo: nil, repeats: false)
    }
    
    @objc func instruction() {
        pushController(withName: "Instruction", context: interfaceColor)
    }
    
    @objc func transfer() {
        pushController(withName: "Transfer", context: userPower)
    }
    
    // Base - Initialization - Base
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
    }
    
    override func willActivate() {
        super.willActivate()
        
        if userReturnsToPreviousInterface == true {
            getInstruction()
        }
        
        if firstWorkout == true {
            getTransfer()
        }
        
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
}
