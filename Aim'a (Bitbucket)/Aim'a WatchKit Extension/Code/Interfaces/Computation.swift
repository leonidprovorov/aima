// Aim'a.
// Based on SpeedySloth by Apple Inc.
// Licensed under the GNU Affero General Public License.
// Created by Leonid Provorov on 20/11/15, Alupka, Crimea.
// In memory of my farther Alexander Provorov (1956 - 2009).
// Copyright © 2019 Leonid Provorov. All rights reserved.

// * Project configuration and code is under development to convenient 1.1 version.

import WatchKit
import Foundation
import HealthKit

class Computation: WKInterfaceController, HKWorkoutSessionDelegate, HKLiveWorkoutBuilderDelegate {
    
    // Storyboard - IBOutles - Storyboard
    @IBOutlet weak var time: WKInterfaceLabel!
    @IBOutlet weak var power: WKInterfaceLabel!
    @IBOutlet weak var beats: WKInterfaceLabel!
    
    @IBOutlet weak var workoutTimer: WKInterfaceTimer!
    @IBOutlet weak var powerValue: WKInterfaceLabel!
    @IBOutlet weak var userPulse: WKInterfaceLabel!
    
    @IBOutlet weak var tempo: WKInterfaceLabel!
    
    @IBOutlet weak var panGesture: WKPanGestureRecognizer!
    
    // Storyboard - IBActions - Storyboard
    @IBAction func panGestureRecognizer(_ sender: Any) { }
    
    @IBAction func done() {
        indicatorTimer.invalidate()
        
        userRetouchCatalyst = false
        userReachComputationPoint = false
        indicator = true
        oneSound = true
        checkPulse = true
        firstWorkout = true
        stopWorkout()
        dismiss()
    }
    
    // Algorithm - Major Constants and Variables - Algorithm
    var sensorEnergy: Double = 0.00
    var sensorPulse: Int = 0
    
   
    
    // HealthKit - Major Constants and Variables - HealthKit
    let healthStore: HKHealthStore = HKHealthStore()
    
    var workoutConfiguration: HKWorkoutConfiguration = HKWorkoutConfiguration()
    
    var liveWorkoutDataSource: HKLiveWorkoutDataSource!
    
    // System - Major Functions - System
    func powerScreenFormat(_ quantity: Double) -> String {
        
        let formatString = NSLocalizedString("%0.2f", comment: "Label for numeric value with unit")
        let powerString = NSLocalizedString("User Power", comment: "Display unit for user power")
        
        return String(format: formatString, quantity, powerString)
        
    }
    
    func formatUserAveragePulse() -> Int {
        
        var fixedPulse: Int = 0
        
        switch userTakePulse {
            
        case 1:
            fixedPulse = userFixedPulse
            
        case 2:
            fixedPulse = userFixedPulse
            
        default:
            fixedPulse = sensorPulse
        
        }
        
        return fixedPulse

    }
    
    
    // Algorithm - Major Functions - Algorithm
    func getIndicatorTimer() {
        indicatorTimer = Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector(indicatorTime), userInfo: nil, repeats: true)
    }
    
    @objc func indicatorTime() {
        
        if sensorPulse > 90 {
            tempo.setText("Calm down: pulse is over 90B")
            if oneSound == true {
                WKInterfaceDevice.current().play(.notification)
                oneSound = false
            }
        } else {
            indicatorTimer.invalidate()
            checkPulse = false
            oneSound = true
        }
        
    }
    
    func getComputationTimer() {
        computationTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(computationTime), userInfo: nil, repeats: true)
    }
    
    func getComputation() {
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(computation), userInfo: nil, repeats: false)
    }
    
    @objc func computationTime() {
    
        if computationCycles != 0 {
            computationCycles -= 1
        } else {
            computationCycles = 60
        }
        
        let computationLapTime: Int = 60
        
        workoutAverageTime += computationLapTime
        
        getComputation()
        
    }
    
    @objc func computation() {
        getUserAveragePower()
    }
    
    func getUserAveragePower() {
        
        let airPower: Double = 310.00
        let earthPower: Double = 155.00
        let firePower: Double = userProtein
        
        let currentEnergy: Double = sensorEnergy
        var recentEnergy: Double = 0.00
        
        let startComputationCycles: Int = 59
        
        switch computationCycles {
            
        case startComputationCycles:
            recentEnergy = sensorEnergy
            
        default:
            
            if userAveragePower >= 9.5 {
                workoutTimer.stop()
                powerValue.setText("10")
                
                if sensorPulse >= 100 {
                    userPulse.setText("\(sensorPulse)")
                } else {
                    userPulse.setText("0\(sensorPulse)")
                }
                
                if userReachComputationPoint == true {
                    WKInterfaceDevice.current().play(.notification)
                    userReachComputationPoint = false
                }
                
                tempo.setText("Great job!")
                
                checkPulse = true
                
                break
            }

            if userPower == "Air Power" {
                userAveragePower += (currentEnergy - recentEnergy) / airPower
            }
            
            if userPower == "Earth Power" {
                userAveragePower += (currentEnergy - recentEnergy) / earthPower
            }
            
            if userPower == "Fire Power" {
                userAveragePower += (currentEnergy - recentEnergy) / firePower
            }
            
            powerValue.setText(powerScreenFormat(userAveragePower))
            
            recentEnergy = sensorEnergy
            
        }
    }
    
    func getUserAveragePulse() {
        
        if sensorPulse >= 100 {
            userPulse.setText("\(sensorPulse)")
        } else {
            userPulse.setText("0\(sensorPulse)")
        }
        
        if checkPulse == false {
            
        let currentPulse = sensorPulse
            
        userAveragePulse = currentPulse
        
        switch userPower {
            
        case "Earth Power":
            
            if currentPulse >= 10 {
                if indicator == true {

                    tempo.setText("Increase tempo to 120B")
                    
                    // second lap
                    if oneSound == true {
                        WKInterfaceDevice.current().play(.start)
                    }
                    //
                    
                     indicator = false
                }
                
            } else { return }
            
            if currentPulse >= 120 {
                
                DispatchQueue.main.async {
                    if userReachComputationPoint == false {
                        self.getComputationTimer()
                        userReachComputationPoint = true
                        userTakePulse = 1
                        userFixedPulse = self.sensorPulse
                    }
                }
                
                if indicator == false {
                    
                    // second lap
                    if oneSound == true {
                        WKInterfaceDevice.current().play(.directionUp)
                        tempo.setText("Keep your tempo")
                    }
                    
                    oneSound = false
                    //
                    
                } else { return }
                
            } else { return }
            
            if currentPulse >= 150 {
                
                
                if oneSound == false {
                    indicator = true
                    WKInterfaceDevice.current().play(.directionDown)
                    tempo.setText("Decrease tempo to 120B")
                    userTakePulse = 2
                    userFixedPulse = sensorPulse
                    oneSound = true
                }
                
                
                
            } else { return }
            
        case "Fire Power":
            
            if currentPulse >= 10 {
                if indicator == true {
                    
                    tempo.setText("Increase tempo to 150B")
                    
                    // second lap
                    if oneSound == true {
                        WKInterfaceDevice.current().play(.start)
                    }
                    //
                    
                    indicator = false
                }
                
            } else { return }
            
            if currentPulse >= 150 {
                
                DispatchQueue.main.async {
                    if userReachComputationPoint == false {
                        self.getComputationTimer()
                        userReachComputationPoint = true
                        userFixedPulse = self.sensorPulse
                        userTakePulse = 1
                    }
                }
                
                if indicator == false {
                    
                    // second lap
                    if oneSound == true {
                        WKInterfaceDevice.current().play(.directionUp)
                        tempo.setText("Keep your tempo")
                        oneSound = false
                    }
                    //
                    
                } else { return }
                
            } else { return }
            
            if currentPulse >= 180 {
                
                
                if oneSound == false {
                    indicator = true
                    WKInterfaceDevice.current().play(.directionDown)
                    tempo.setText("Decrease tempo to 150B")
                    userTakePulse = 2
                    userFixedPulse = sensorPulse
                    oneSound = true
                }
                
            } else { return }
            
            
        default:
        
            if currentPulse >= 10 {
                if indicator == true {
                    
                    tempo.setText("Increase tempo to 90B")
                    
                    // second lap
                    if oneSound == true {
                        WKInterfaceDevice.current().play(.start)
                    }
                    //
                    
                    indicator = false
                }
                
            } else { return }
            
            if currentPulse >= 90 {
                
                DispatchQueue.main.async {
                    if userReachComputationPoint == false {
                        self.getComputationTimer()
                        userReachComputationPoint = true
                        userTakePulse = 1
                        userFixedPulse = self.sensorPulse
                    }
                }
                
                if indicator == false {
                    
                    // second lap
                    if oneSound == true {
                        WKInterfaceDevice.current().play(.directionUp)
                        tempo.setText("Keep your tempo")
                        oneSound = false
                    }
                    //
                    
                } else { return }
                
            } else { return }
            
            if currentPulse >= 120 {
                
                if oneSound == false {
                    indicator = true
                    WKInterfaceDevice.current().play(.directionDown)
                    tempo.setText("Decrease tempo to 90B")
                    userTakePulse = 2
                    userFixedPulse = sensorPulse
                    oneSound = true
                }
                
            } else { return }
            
            
            }
        }
    }
    
    // HealthKit - Major Functions - HealthKit
    func getWorkoutConfiguration() {
        workoutConfiguration.activityType = .other
        workoutConfiguration.locationType = .unknown
    }
    
    func getLiveWorkoutDataSource() {
        
        liveWorkoutDataSource = HKLiveWorkoutDataSource(healthStore: healthStore, workoutConfiguration: workoutConfiguration)
        
        if let activeEnergyBurned = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned) {
        
            let datePredicate = HKQuery.predicateForSamples(withStart: isReach, end: nil, options: .strictStartDate)
            let devicePredicate = HKQuery.predicateForObjects(from: [HKDevice.local()])
            let queryPredicate = NSCompoundPredicate(andPredicateWithSubpredicates:[datePredicate, devicePredicate])
            
            liveWorkoutDataSource.enableCollection(for: activeEnergyBurned, predicate: queryPredicate)
        }
        
        if let heartRate = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate) {
            liveWorkoutDataSource.enableCollection(for: heartRate, predicate: nil)
        }
        
        liveWorkoutBuilder.dataSource = liveWorkoutDataSource
        
    }
    
    func startWorkout() {
    
        getWorkoutConfiguration()
        
        do {
            workout = try? HKWorkoutSession(healthStore: healthStore, configuration: workoutConfiguration)
            liveWorkoutBuilder = workout.associatedWorkoutBuilder()
        }
        
        workout.delegate = self
        liveWorkoutBuilder.delegate = self
        
        getLiveWorkoutDataSource()
        
        workout.startActivity(with: Date())
        liveWorkoutBuilder.beginCollection(withStart: Date()) { (success, error) in }
        
    }
    
    func stopWorkout() {
        
        computationTimer.invalidate()
        computationCycles = 60
        
        let database: Database = Database()
        
        switch userPower {
            
        case "Earth Power":
            database.writeToDatabase(earthAverageTime: workoutAverageTime, earthAveragePower: userAveragePower, earthAveragePulse: formatUserAveragePulse())
            
           // database.writeToDatabase(airPower: nil, earthPower: userPower, firePower: nil)
            
            database.write(hasSentData: 1)
            
        case "Fire Power":
            database.writeToDatabase(fireAverageTime: workoutAverageTime, fireAveragePower: userAveragePower, fireAveragePulse: formatUserAveragePulse())
            
            //database.writeToDatabase(airPower: nil, earthPower: nil, firePower: userPower)
            
            database.write(hasSentData: 2)
            
        default:
            database.writeToDatabase(airAverageTime: workoutAverageTime, airAveragePower: userAveragePower, airAveragePulse: formatUserAveragePulse())
            
           // database.writeToDatabase(airPower: userPower, earthPower: nil, firePower: nil)
            
            database.write(hasSentData: 0)
        }
        
        workout.end()
        
        liveWorkoutBuilder.endCollection(withEnd: Date()) { (success, error) in
            liveWorkoutBuilder.finishWorkout { (workout, error) in }
        }
        
        userAveragePulse = 0
        workoutAverageTime = 0
        userAveragePower = 0.0
        userTakePulse = 0
        
    }
    
    // HKWorkoutSessionDelegate - Major Functions - HKWorkoutSessionDelegate
    func workoutSession(_ workoutSession: HKWorkoutSession, didChangeTo toState: HKWorkoutSessionState, from fromState: HKWorkoutSessionState, date: Date) { }
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didFailWithError error: Error) { }
    
    // HKLiveWorkoutBuilderDelegate - Major Functions - HKLiveWorkoutBuilderDelegate
    func workoutBuilderDidCollectEvent(_ workoutBuilder: HKLiveWorkoutBuilder) { }
    
    func workoutBuilder(_ workoutBuilder: HKLiveWorkoutBuilder, didCollectDataOf collectedTypes: Set<HKSampleType>) {
        
        guard let heartRate = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate) else { return }
        
        if userReachComputationPoint == true {
            
            guard let activeEnergyBurned = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned) else { return }
            
            if collectedTypes.contains(activeEnergyBurned) {
                
                if let activeEnergyBurnedQuantity = workoutBuilder.statistics(for: activeEnergyBurned)?.sumQuantity() {
                    
                    let quantityUnit: HKUnit = HKUnit(from: "kcal")
                    let quantityValue: Double = Double(activeEnergyBurnedQuantity.doubleValue(for: quantityUnit))
                
                    DispatchQueue.main.async {
                        self.sensorEnergy = quantityValue
                    }
                    
                }
            }
        }
        
        if collectedTypes.contains(heartRate) {
            
            if let heartRateQuantity = workoutBuilder.statistics(for: heartRate)?.mostRecentQuantity() {
                
                let quantityUnit: HKUnit = HKUnit(from: "count/min")
                let quantityValue: Int = Int(heartRateQuantity.doubleValue(for: quantityUnit))
                
                DispatchQueue.main.async {
                    self.sensorPulse = quantityValue
                }
                
                getUserAveragePulse()
                
            }
        }
    }
    
    // Computation - Initialization - Computation
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        switch context as? String {
            
        case "Space Gray":
            let gray: UIColor = UIColor(red: 158/255, green: 157/255, blue: 159/255, alpha: 1.0)
            
            time.setTextColor(gray)
            power.setTextColor(gray)
            beats.setTextColor(gray)
            
        case "Pink Gold":
            let gold: UIColor = UIColor(red: 254/255, green: 229/255, blue: 218/255, alpha: 1.0)
            
            time.setTextColor(gold)
            power.setTextColor(gold)
            beats.setTextColor(gold)
            
        default:
            let silver: UIColor = UIColor(red: 231/255, green: 231/255, blue: 233/255, alpha: 1.0)
            
            time.setTextColor(silver)
            power.setTextColor(silver)
            beats.setTextColor(silver)
            
        }
        
        switch userPower {
            
        case "Earth Power":
            let moss: UIColor = UIColor(red: 0/255, green: 144/255, blue: 81/255, alpha: 1.0)
            
            tempo.setTextColor(moss)
            powerValue.setTextColor(moss)
            
        case "Fire Power":
            let customRed: UIColor = UIColor(red: 205/255, green: 10/255, blue: 55/255, alpha: 1.0)
            
            tempo.setTextColor(customRed)
            powerValue.setTextColor(customRed)
            
        default:
            let aqua: UIColor = UIColor(red: 0/255, green: 150/255, blue: 255/255, alpha: 1.0)
            
            tempo.setTextColor(aqua)
            powerValue.setTextColor(aqua)
            
        }
        
        userRetouchCatalyst = true
        
        startWorkout()
        
        getIndicatorTimer()
        
        let workoutTime: Double = 3660.00
        let workoutDate: Date = Date(timeIntervalSinceNow: workoutTime)
        
        workoutTimer.setDate(workoutDate)
        workoutTimer.start()
        
        setTitle("⚙️")
        
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
}
