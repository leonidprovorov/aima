// Aim'a.
// Based on SpeedySloth by Apple Inc.
// Licensed under the GNU Affero General Public License.
// Created by Leonid Provorov on 20/11/15, Alupka, Crimea.
// In memory of my farther Alexander Provorov (1956 - 2009).
// Copyright © 2019 Leonid Provorov. All rights reserved.

// * Project configuration and code is under development to convenient 1.1 version.

import UIKit
import HealthKit
import WatchConnectivity

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
   
    var window: UIWindow?
    
    // HealthKit - Major Functions - HealthKit
    func applicationShouldRequestHealthAuthorization(_ application: UIApplication) {
        let healthStore: HKHealthStore = HKHealthStore()
        healthStore.handleAuthorizationForExtension { (success, error) in }
    }

    // UIApplicationDelegate - Major Functions - UIApplicationDelegate
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
//        if UIDevice.current.orientation.isPortrait {
//
//            switch UIDevice.getCurrentiPhone() {
//
//            case .iPhoneXsMax:
//                iPhone = "iPhone Xs Max"
//
//            case .iPhoneXs:
//                iPhone = "iPhone Xs"
//
//            case .iPhoneXr:
//                iPhone = "iPhone Xr"
//
//            case .iPhone8Plus:
//                iPhone = "iPhone 8 Plus"
//
//            case .iPhone8:
//                iPhone = "iPhone 8"
//
//            case .iPhoneSE:
//                iPhone = "iPhone SE"
//
//            default:
//                break
//
//            }
//        }

       return true
//
    }

    func applicationWillResignActive(_ application: UIApplication) { }

    func applicationDidEnterBackground(_ application: UIApplication) { }

    func applicationWillEnterForeground(_ application: UIApplication) { }
    
    func applicationWillTerminate(_ application: UIApplication) { }

    func applicationDidBecomeActive(_ application: UIApplication) { }
    
}

