// Aim'a.
// Based on SpeedySloth by Apple Inc.
// Licensed under the GNU Affero General Public License.
// Created by Leonid Provorov on 20/11/15, Alupka, Crimea.
// In memory of my farther Alexander Provorov (1956 - 2009).
// Copyright © 2019 Leonid Provorov. All rights reserved.

// * Project configuration and code is under development to convenient 1.1 version.

import UIKit
import WatchConnectivity
import HealthKit

class View: UIViewController, WCSessionDelegate {
    
    
    @IBOutlet weak var timeValue: UILabel!
    @IBOutlet weak var powerValue: UILabel!
    @IBOutlet weak var beatsValue: UILabel!

    
    // Storyboard - IBOutles - Storyboard
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var power: UILabel!
    @IBOutlet weak var beats: UILabel!
    
    @IBOutlet weak var start: UIButton!
    
    var somePower: String = "Power"
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
 
    @IBAction func startWatch(_ sender: Any) {
         print("Data")
        
         database.getData(time: timeValue, power: powerValue, beats: beatsValue)
    }
    
   
    
//    func getLabels() {
//
//        switch iPhone {
//
//        case "iPhone Xs Max":
//
//
//        case "iPhone Xs":
//
//        case "iPhone Xr":
//
//        case "iPhone 8 Plus":
//
//        case "iPhone 8":
//
//        case "iPhone SE":
//
//        default:
//            break
//
//        }
//
//    }
    
    let database: Database = Database()
    
    // WatchConnectivity - Major Functions - WatchConnectivity
    func getSessionStatus() {
        
        if WCSession.isSupported() {
            wcSession.delegate = self
            wcSession.activate()
        }
        
        
    }

    
    // WCSessionDelegate - Major Functions - WCSessionDelegate
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) { }
    
    func sessionDidBecomeInactive(_ session: WCSession) { }
    
    func sessionDidDeactivate(_ session: WCSession) { }
    
    func session(_ session: WCSession, didReceiveUserInfo userInfo: [String : Any] = [:]) {
        
        if session.activationState == .activated {
            print("activated")
            
            if let airToPower = userInfo["0"] as? Int {
                //database.writeToDatabase(airPower: airToPower, earthPower: nil, firePower: nil)
                database.write(hasSentData: 0)
                print(airToPower)
            }
            
            if let earthToPower = userInfo["1"] as? Int {

               // database.writeToDatabase(airPower: nil, earthPower: earthToPower, firePower: nil)
               database.write(hasSentData: 1)
               print(earthToPower)
              
            }
            
            if let fireToPower = userInfo["2"] as? Int {
               // database.writeToDatabase(airPower: nil, earthPower: nil, firePower: fireToPower)
               
                database.write(hasSentData: 2)
                print(fireToPower)
            }
            
            
            if let airTime = userInfo["Air: average time"] as? Int {
                
                var formattedTime: Int = 0
                
                if airTime >= 60 {
                    formattedTime = airTime / 60
                }
                
                database.writeToDatabase(airAverageTime: formattedTime)
                
                DispatchQueue.main.async {
                    self.timeValue.text = "\(formattedTime)m"
                }
                
            }
            
            if let airPower = userInfo["Air: average power"] as? Double {
                database.writeToDatabase(airAveragePower: airPower)
                
                DispatchQueue.main.async {
                    self.powerValue.text = powerScreenFormat(airPower)
                }
                
            }
            
            if let airPulse = userInfo["Air: average pulse"] as? Int {
                database.writeToDatabase(airAveragePulse: airPulse)
                
                DispatchQueue.main.async {
                    self.beatsValue.text = "\(airPulse)"
                }
                
            }
            

            if let earthTime = userInfo["Earth: average time"] as? Int {
                
                
                var formattedTime: Int = 0
                
                if earthTime >= 60 {
                    formattedTime = earthTime / 60
                }
                
                database.writeToDatabase(earthAverageTime: formattedTime)
                
                DispatchQueue.main.async {
                    self.timeValue.text = "\(formattedTime)m"
                }
            }
            
            if let earthPower = userInfo["Earth: average power"] as? Double {
                database.writeToDatabase(earthAveragePower: earthPower)
                
                DispatchQueue.main.async {
                    self.powerValue.text = powerScreenFormat(earthPower)
                }
            }
            
            if let earthPulse = userInfo["Earth: average pulse"] as? Int {
                database.writeToDatabase(earthAveragePulse: earthPulse)
                
                DispatchQueue.main.async {
                    self.beatsValue.text = "\(earthPulse)"
                }
            }
            
            
            if let fireTime = userInfo["Fire: average time"] as? Int {
                
                var formattedTime: Int = 0
                
                if fireTime >= 60 {
                    formattedTime = fireTime / 60
                }
                
                
                database.writeToDatabase(fireAverageTime: formattedTime)
                
                DispatchQueue.main.async {
                    self.timeValue.text = "\(formattedTime)m"
                }
            }
            
            if let firePower = userInfo["Fire: average power"] as? Double {
                database.writeToDatabase(fireAveragePower: firePower)
                
                DispatchQueue.main.async {
                    self.powerValue.text = powerScreenFormat(firePower)
                }
            }
            
            if let firePulse = userInfo["Fire: average pulse"] as? Int {
                database.writeToDatabase(fireAveragePulse: firePulse)
                
                DispatchQueue.main.async {
                   self.beatsValue.text = "\(firePulse)"
                }
            }
            
        }
    }
    
    
    // View - Initialization - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getSessionStatus()

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
        

}

