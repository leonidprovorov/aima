// Aim'a.
// Based on SpeedySloth by Apple Inc.
// Licensed under the GNU Affero General Public License.
// Created by Leonid Provorov on 20/11/15, Alupka, Crimea.
// In memory of my farther Alexander Provorov (1956 - 2009).
// Copyright © 2019 Leonid Provorov. All rights reserved.

// * Project configuration and code is under development to convenient 1.1 version.

import UIKit
import WatchConnectivity




func powerScreenFormat(_ quantity: Double) -> String {
    
    let formatString = NSLocalizedString("%0.2f", comment: "Label for numeric value with unit")
    let powerString = NSLocalizedString("User Power", comment: "Display unit for user power")
    
    return String(format: formatString, quantity, powerString)
}


func timeScreenFormat(_ quantity: Int) -> String {
    
    let formatString = NSLocalizedString("%0.1fm", comment: "Label for numeric value with unit")
    let timeString = NSLocalizedString("User Time", comment: "Display unit for user time")
    
    return String(format: formatString, quantity, timeString)
    
}

func beatsScreenFormat(_ quantity: Int) -> String {
    
    let formatString = NSLocalizedString("%1f", comment: "Label for numeric value with unit")
    let beatsString = NSLocalizedString("User Beats", comment: "Display unit for user beats")
    
    return String(format: formatString, quantity, beatsString)
    
}



//var iPhone: String = "iPhone"
//
//enum iPhoneTypes {
//    case iPhoneXsMax
//    case iPhoneXs, iPhoneX
//    case iPhoneXr
//    case iPhone8Plus, iPhone7Plus, iPhone6sPlus
//    case iPhone8, iPhone7, iPhone6s
//    case iPhoneSE, iPhone5s
//}
//
//extension UIDevice {
//
//    class func getCurrentiPhone() -> iPhoneTypes {
//
//        let screenBounds: [String : CGRect] = [
//            "iPhone Xs Max"       : CGRect(x: 0, y: 0, width: 1242, height: 2688),
//            "iPhone Xs, iPhone X" : CGRect(x: 0, y: 0, width: 1125, height: 2436),
//            "iPhone Xr"           : CGRect(x: 0, y: 0, width: 828, height: 1792),
//            "iPhone 8 Plus, iPhone 7 Plus, iPhone 6s Plus" : CGRect(x: 0, y: 0, width: 1242, height: 2208),
//            "iPhone 8, iPhone 7, iPhone 6s" : CGRect(x: 0, y: 0, width: 750, height: 1334),
//            "IPhone SE, iPhone 5s"          : CGRect(x: 0, y: 0, width: 640, height: 1136)
//        ]
//
//        let currentScreenBounds: CGRect = UIScreen.main.bounds
//
//        switch currentScreenBounds {
//
//        case screenBounds["iPhone Xs Max"]:
//            return .iPhoneXsMax
//
//        case screenBounds["iPhone Xs, iPhone X"]:
//            return .iPhoneXs
//
//        case screenBounds["iPhone Xr"]:
//            return .iPhoneXr
//
//        case screenBounds["iPhone 8 Plus, iPhone 7 Plus, iPhone 6s Plus"]:
//            return .iPhone8Plus
//
//        case screenBounds["iPhone 8, iPhone 7, iPhone 6s"]:
//            return .iPhone8
//
//        case screenBounds["IPhone SE, iPhone 5s"]:
//            return .iPhoneSE
//
//        default:
//            return .iPhoneXsMax
//            
//        }
//    }
//
//}









// WatchConnectivity - Major Constants and Variables - WatchConnectivity
let wcSession: WCSession = WCSession.default

class Database {
    
    // Database - Major Constants and Variables - Database
    let database: UserDefaults = UserDefaults.standard
    
    var hasData: Int!
    
    func returnAirPower() -> Int? {
        
        var power: Int!
        
//        if database.string(forKey: "1") != nil {
//            database.removeObject(forKey: "1")
//        }
//
//        if database.string(forKey: "2") != nil {
//            database.removeObject(forKey: "2")
//        }
        
        if database.string(forKey: "0") != nil {
            power = database.integer(forKey: "0")
        }
        
        return power
    }
    
    func returnFirePower() -> Int? {
        
        var power: Int!
//
//        if database.string(forKey: "0") != nil {
//            database.removeObject(forKey: "0")
//        }
//
//        if database.string(forKey: "1") != nil {
//            database.removeObject(forKey: "1")
//        }
        
        if database.string(forKey: "2") != nil {
            power = database.integer(forKey: "2")
        }
        
        
        return power
    }
    
    func returnEarthPower() -> Int? {
        
        var power: Int!
        
//        if database.string(forKey: "0") != nil {
//            database.removeObject(forKey: "0")
//        }
//
//        if database.string(forKey: "2") != nil {
//            database.removeObject(forKey: "2")
//        }
//
        if database.string(forKey: "1") != nil {
             power = database.integer(forKey: "1")
        }
        
        return power
    }
    
    var userPower: [String : Any] = [
        "Air Power" : String(),
        "Earth Power" : String(),
        "Fire Power" : String()
    ]
    
   
    
    func getData(time: UILabel?, power: UILabel?, beats: UILabel?) {
       
        let airTime: Int = database.integer(forKey: "Air: average time")
        let airPower: Double = database.double(forKey: "Air: average power")
        let airPulse: Int = database.integer(forKey: "Air: average pulse")
        
        let earthTime: Int = database.integer(forKey: "Earth: average time")
        let earthPower: Double = database.double(forKey: "Earth: average power")
        let earthPulse: Int = database.integer(forKey: "Earth: average pulse")
        
        let fireTime: Int = database.integer(forKey: "Fire: average time")
        let firePower: Double = database.double(forKey: "Fire: average power")
        let firePulse: Int = database.integer(forKey: "Fire: average pulse")
        
        if returnAirPower() == 0 {
            time?.text = "\(airTime)m"
            power?.text = powerScreenFormat(airPower)
            beats?.text = "\(airPulse)"
        }
        
        if returnEarthPower() == 1 {
            time?.text = "\(earthTime)m"
            power?.text = powerScreenFormat(earthPower)
            beats?.text = "\(earthPulse)"
        }
        
        if returnFirePower() == 2 {
            time?.text = "\(fireTime)m"
            power?.text = powerScreenFormat(firePower)
            beats?.text = "\(firePulse)"
        }
    
    }
    
    
    
    var airPower: [String : Any] = [
        "Air: average time" : Int(),
        "Air: average power" : Double(),
        "Air: average pulse" : Int()
    ]
    
    var earthPower: [String : Any] = [
        "Earth: average time" : Int(),
        "Earth: average power" : Double(),
        "Earth: average pulse" : Int()
    ]
    
    var firePower: [String : Any] = [
        "Fire: average time" : Int(),
        "Fire: average power" : Double(),
        "Fire: average pulse" : Int()
    ]
    
    // Database - Major Functions - Database
    func write(hasSentData: Int?) {
        if hasSentData == 0 {
            database.set(hasSentData, forKey: "0")
        }
        
        if hasSentData == 1 {
            database.set(hasSentData, forKey: "1")
        }
        
        if hasSentData == 2 {
            database.set(hasSentData, forKey: "2")
        }
        
    }
    
    
    func writeToDatabase(airPower: String?, earthPower: String?, firePower: String?) {
        
        if airPower != nil {
            database.set(airPower, forKey: "Air Power")
        } else { return }
        
        if earthPower != nil {
            database.set(earthPower, forKey: "Earth Power")
        
        } else { return }
        
        if firePower != nil {
            database.set(firePower, forKey: "Fire Power")
        
        } else { return }
        
    }
    
    func writeToDatabase(airAverageTime: Int?) {
        
        if airAverageTime != nil {
            database.set(airAverageTime, forKey: "Air: average time")
        } else { return }
        
    }
    
    func writeToDatabase(airAveragePower: Double?) {
        
        if airAveragePower != nil {
            database.set(airAveragePower, forKey: "Air: average power")
        } else { return }
        
    }
    
    
    func writeToDatabase(airAveragePulse: Int?) {
        
        if airAveragePulse != nil {
            database.set(airAveragePulse, forKey: "Air: average pulse")
        } else { return }
        
    }
    
    
    
    
    
    func writeToDatabase(earthAverageTime: Int?) {
        
        if earthAverageTime != nil {
            database.set(earthAverageTime, forKey: "Earth: average time")
        } else { return }
        
    }
    
    func writeToDatabase(earthAveragePower: Double?) {
        
        if earthAveragePower != nil {
            database.set(earthAveragePower, forKey: "Earth: average power")
        } else { return }
        
    }
    
    
    func writeToDatabase(earthAveragePulse: Int?) {
        
        if earthAveragePulse != nil {
            database.set(earthAveragePulse, forKey: "Earth: average pulse")
        } else { return }
        
    }
    
    
    

    
    func writeToDatabase(fireAverageTime: Int?) {
        
        if fireAverageTime != nil {
            database.set(fireAverageTime, forKey: "Fire: average time")
        } else { return }
        
    }
    
    func writeToDatabase(fireAveragePower: Double?) {
        
        if fireAveragePower != nil {
            database.set(fireAveragePower, forKey: "Fire: average power")
        } else { return }
        
    }
    
    
    func writeToDatabase(fireAveragePulse: Int?) {
        
        if fireAveragePulse != nil {
            database.set(fireAveragePulse, forKey: "Fire: average pulse")
        } else { return }
        
    }
    
    
    
}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

